import torch
from model import DefaultInpaintingTrainingModule    
from PIL import Image
import numpy as np
from torch.utils.data._utils.collate import default_collate
import cv2

def transforms(img):
    if img.ndim == 3:
        img = np.transpose(img, (2, 0, 1))
    out_img = img.astype('float32') / 255
    return out_img

def img_load(file_img,file_mask):
    # 读取图像
    img = Image.open(file_img)
    img = img.convert('RGB')
    img = np.array(img)
    img = np.transpose(img, (2, 0, 1))
    img = img.astype('float32') / 255

    # 读取蒙版
    mask = Image.open(file_mask)
    mask = mask.convert('L')
    mask = np.array(mask)
    mask = mask.astype('float32') / 255

    return img,mask

def ceil_modulo(x, mod):
    if x % mod == 0:
        return x
    return (x // mod + 1) * mod

def pad_img_to_modulo(img, mod):
    channels, height, width = img.shape
    out_height = ceil_modulo(height, mod)
    out_width = ceil_modulo(width, mod)
    return np.pad(
        img, ((0, 0), (0, out_height - height), (0, out_width - width)),
        mode='symmetric')

def preprocessing(img,mask):
    result = dict(image=img, mask=mask[None, ...]) 

    # 记录边缘扩展前的尺寸
    result['unpad_to_size'] = result['image'].shape[1:]

    # 进行边缘扩展
    result['image'] = pad_img_to_modulo(result['image'],8)
    result['mask'] =  pad_img_to_modulo(result['mask'],8)
    return result


def model_load(file_model,net):
    # 创建一个空的字典，用来进行填充
    state_dict = net.state_dict()

    # 加载保存的模型的内容
    save_state_dic = torch.load(file_model, map_location='cpu')

    new_state_dict = {}
    # 利用保存的模型对 空字典进行填充
    for k,v in state_dict.items():
        try:
            new_state_dict[k] = save_state_dic[k]
        except:
            print(k,'not find')
            new_state_dict[k] = v
    
    net.load_state_dict(new_state_dict)
    return net

def get_mask(file_in,color_maks=(10,255,10),file_out="m_mask.png"):
    img = cv2.imread(file_in)
    c = np.expand_dims(color_maks,[0,1])
    mask = np.sum(img-c,axis=-1)
    mask = np.array(mask==0,dtype=np.uint8)*255
    mask = cv2.medianBlur(mask, 7)
    mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
    cv2.imwrite(file_out,mask)
    return file_out
    


if __name__ == "__main__":
    
    device = 'cpu'
    file_model = "pytorch_model.pt"
    
    ## 已有蒙版
    # file_img = "image_inpainting.png"
    # file_mask = "image_inpainting_mask.png"
    
    # 自定义蒙版
    file_img = "img.bmp"
    file_mask = "m_mask.png"
    get_mask("mask5.bmp",(34,255,34))
    
    # 图像读取（通道转换、数值scale）
    img,mask = img_load(file_img,file_mask)

    # 数据预处理 （padding 长、宽被 8 整除）
    result = preprocessing(img,mask)

    # 数据打包变成pytorch支持的格式
    batch = default_collate([result])

    # 模型加载
    net = DefaultInpaintingTrainingModule(model_dir=file_model, predict_only=True) 
    net = model_load(file_model,net)
    net.eval()
  
    with torch.no_grad():
        
        batch['mask'] = (batch['mask'] > 0) * 1
        batch = net(batch)
        cur_res = batch['inpainted'][0].permute(
                    1, 2, 0).detach().cpu().numpy()
        unpad_to_size = batch.get('unpad_to_size', None)
        if unpad_to_size is not None:
            orig_height, orig_width = unpad_to_size
            cur_res = cur_res[:orig_height, :orig_width]
        cur_res = np.clip(cur_res * 255, 0, 255).astype('uint8')
        cur_res = cv2.cvtColor(cur_res, cv2.COLOR_RGB2BGR)
    
        cv2.imwrite("out4.png",cur_res)