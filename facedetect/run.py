import streamlit as st
import os
import numpy as np
import dlib
import cv2
from src.util import add_cartoon_eye,add_cnter_eye,add_glasses,add_hat

if "flag_video" not in st.session_state:  
    st.session_state.flag_video = False


def start_video():
    st.session_state.flag_video = True

def stop_video():
    st.session_state.flag_video = False

@st.cache_resource
def model_load():
    # 创建人脸检测器
    det_face = dlib.get_frontal_face_detector()
    # 加载标志点检测器
    det_landmark = dlib.shape_predictor("shape_predictor_68_face_landmarks_GTX.dat")  # 68点
    # 打开摄像头
    cap = cv2.VideoCapture(0)
    t_left_eye = cv2.imread(os.path.join('data','eye',"left-eye.bmp"))
    t_right_eye = cv2.imread(os.path.join('data','eye',"right-eye.bmp"))
    center_eye = cv2.imread(os.path.join('data','eye',"center-eye.bmp"))
    img_glasses = cv2.imread(os.path.join('data','eye',"glasses.bmp"))
    img_hat1 = cv2.imread(os.path.join('data','hat',"hat1.bmp"))
    img_hat2 = cv2.imread(os.path.join('data','hat',"hat2.bmp"))
    return det_face,det_landmark,cap,t_left_eye,t_right_eye,center_eye,img_glasses,img_hat1,img_hat2

def showflag():
    print(st.session_state.flag_base)
    print(st.session_state.flag_hat)
    print(st.session_state.flag_eye)

det_face,det_landmark,cap,t_left_eye,t_right_eye,center_eye,img_glasses,img_hat1,img_hat2 = model_load()

st.title("人脸检测/人脸特效展示")

st.radio("关键点",['显示','不显示'],key='flag_base',on_change=showflag,horizontal=True,index=1)
st.radio("帽子",['帽子1','帽子2','无'],key='flag_hat',on_change=showflag,horizontal=True,index=2)
st.radio("眼睛",['卡通眼','中心眼','眼镜','无'],key='flag_eye',on_change=showflag,horizontal=True,index=3)
st.write("--------------------------------------------------------------------")

# 开启关闭摄像头
col1, col2 = st.columns(2)
with col1:
    st.button("摄像头打开",on_click=start_video)
with col2:
    st.button("摄像头停止",on_click=stop_video)

# 占位符，用来显示图像
st_show = st.empty()

try:
    while st.session_state.flag_video:
        
        # 读取一帧图像
        success, img = cap.read()

        # 转换为灰度
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # 检测人脸区域
        face_rects = det_face(gray, 0)

        for ret in face_rects:
            # 标志点检测
            landmarks = det_landmark(gray, ret)
            
            parts = landmarks.parts()
            
            if st.session_state.flag_base == "显示":
                # 画出人脸区域
                cv2.rectangle(img, (ret.left(),ret.top()), (ret.right(),ret.bottom()), (255, 0, ), 3)
                # 画出 关键点
                for part in landmarks.parts():
                    pt = (part.x,part.y)
                    cv2.circle(img, pt, 2, (0,0,255),-1)

            
            if st.session_state.flag_eye =="眼镜":
                img = add_glasses(img,img_glasses,parts)
            elif st.session_state.flag_eye =="卡通眼":
                img = add_cartoon_eye(img,t_left_eye,t_right_eye,parts)
            elif st.session_state.flag_eye =="中心眼":
                img = add_cnter_eye(img,center_eye,parts)
                
            if st.session_state.flag_hat =="帽子1":
                img = add_hat(img,img_hat1,parts)
            elif st.session_state.flag_hat =="帽子2":
                img= add_hat(img,img_hat2,parts)

        # 图像显示
        img_show = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

        st_show.image(img_show)
except Exception as e:
    print(e)
    # cap.release() 




