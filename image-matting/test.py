import tensorflow as tf 
tf.compat.v1.disable_eager_execution()
import numpy as np
from PIL import Image
import cv2
class ImageMatting():
    def __init__(self,file_model,str_device):

        self.file_model = file_model
        self.device = str_device
        with tf.device(self.device):
            self.config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
            self.session = tf.compat.v1.Session(config=self.config)
        with self.session.as_default():
            with tf.compat.v1.gfile.FastGFile(self.file_model, 'rb') as f:
                graph_def = tf.compat.v1.GraphDef()
                graph_def.ParseFromString(f.read())
                tf.import_graph_def(graph_def, name='')
                self.output = self.session.graph.get_tensor_by_name(
                        'output_png:0')
                self.input_name = 'input_image:0'
    def preprocessing(self,file_img):
        img = Image.open(file_img)
        img = img.convert('RGB')
        img = np.array(img)
        img = img.astype(float)
        result = {'img': img}
        return result
    
    def forward(self, input):
        with self.session.as_default():
            feed_dict = {self.input_name: input['img']}
            output_img = self.session.run(self.output, feed_dict=feed_dict)
            return output_img


if __name__ == "__main__":
    file_model = "tf_graph.pb"
    device = '/CPU:0'
    # 实例构建
    m_ImageMatting = ImageMatting(file_model,device)
    
    # 图像预处理
    img_proccessed = m_ImageMatting.preprocessing('1.png')
    
    # 图像抠图
    output_img = m_ImageMatting.forward(img_proccessed)
     
    # 读取蒙版,保存
    mask = output_img[:,:,-1]
    mask = mask.astype(np.uint8)
    cv2.imwrite("mask.png",mask)

    # 图像保存 png格式 增加Alpha
    output_img = cv2.cvtColor(output_img, cv2.COLOR_RGBA2BGRA)
    cv2.imwrite("out.png",output_img)


    # 背景切换
    img_in = cv2.imread('out.png',cv2.IMREAD_UNCHANGED)
    h,w,c = img_in.shape
    mask = np.zeros([h,w,1])
    mask[:,:,0] = img_in[:,:,-1]
    img = img_in[:,:,0:3]
    mask = mask/255.0
    
    img_bg = cv2.imread("bg.png")
    img_bg = cv2.resize(img_bg,(w,h))


    out_img = img*mask + img_bg*(1-mask)
    out_img = out_img.astype(np.uint8)

    cv2.imwrite("out_img.png",out_img)










    


