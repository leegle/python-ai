import streamlit as st
from src.test_knn import M_KNN

import numpy as np
import time


def get_str_errors(list_error):
    str_errors = ""
    for i in list_error:
        str_errors  = str_errors + str(i) +","
    
    return str_errors




@ st.cache_resource
def load_model():
    knn = M_KNN()
    return knn 

m_knn = load_model()

# 添加一个侧边栏，用于调整KNN算法中的k值
st.sidebar.header('KNN参数')
k = st.sidebar.slider('K近邻', 3, 11, 5,2)
ratio = st.sidebar.slider('训练数据比例', 0.0, 1.0, 0.7,0.1)


acc_show = m_knn.KNN_testSet()

@st.cache_data
def change_callback(k,ratio):
    print("----------run--------------")
    m_knn.set_K(k)
    m_knn.set_ratio(ratio)
    acc_show = m_knn.KNN_testSet()
    fig = m_knn.plot_train_datas(0)
    str_errors = get_str_errors(m_knn.list_error)
    return acc_show,fig,str_errors
    

st.header('KNN分类器')


# acc_show,fig,str_errors = change_callback([k,ratio])
acc_show,fig_train,str_errors = change_callback(k,ratio)
st.write("检测结果%2f%%"%(acc_show))

N_test = len(m_knn.test_labs)

col1, col2 = st.columns(2)

with col1:
    list_labs = [str(i) for i in range(N_test)]
    i_sel = st.selectbox("测试样本",list_labs)
with col2:
    option = st.radio( label = '显示模式', options=('单独', '自动'))

st.write("错误样本：%s"%(str_errors))


stframe = st.empty()
    
if option == '自动':
    
    for i in range(N_test):
        try:
            fig = m_knn.plot_train_datas(sel_i=int(i))
            stframe.pyplot(fig = fig)
            time.sleep(0.5)
    
        except:
            stframe.pyplot(fig = fig_train)
            time.sleep(0.5)
            continue
        

if option == '单独':
    try:
        fig = m_knn.plot_train_datas(sel_i=int(i_sel))
        stframe.pyplot(fig = fig)
    except:
        stframe.pyplot(fig = fig_train)


    


    





