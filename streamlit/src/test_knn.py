import numpy as np
import operator
import  matplotlib.pyplot as plt
import pandas as pd
from sklearn.decomposition import PCA
import time
from PIL import Image
list_color = ['b','g','r','c','m','y','k','w']
list_marker = ['*',',','o','^','1','+','x','D']


class M_KNN:
    def __init__(self) -> None:
        self.k =5
        self.file = 'iris.data'
        self.ratio = 0.7
        self.read_data()
        self.split_datas()
        self.datas_show = self.creatDatasForShow(self.datas)
        self.dic_show = self.get_dic_show()
        self.list_error = []

    def set_ratio(self,ratio):
        self.ratio = ratio
        self.split_datas()
    
    def set_file(self,file):
        self.file = file
        self.read_data()
        self.split_datas()

    def creatDatasForShow(self,datas):
        pca  =PCA(n_components=2)
        pca.fit(datas)
        return pca.transform(datas)


    def set_K(self,K):
        self.k = K
    
    def get_dic_show(self):
        dic_show = dict()
        unique_labs = np.unique(self.labs).tolist()
        for i, lab in enumerate(unique_labs) :
            dic_show[lab] = (list_color[i],list_marker[i])
        return dic_show

    def read_data(self):
        with open(self.file,'r',encoding='utf-8') as f:
            lines = f.read().splitlines()
    
        datas = [line.split(',')[:-1] for line in lines]
        labs = [line.split(',')[-1] for line in lines]
        self.datas = np.array(datas,dtype=np.float32)
        self.labs = np.array(labs,dtype=str)
       
    
    def split_datas(self): 
        N,D = np.shape(self.datas)
        N_train = int(N*self.ratio)
        index_random = np.random.permutation(N)
        self.index_train = index_random[:N_train]
        self.index_test = index_random[N_train:]

        self.train_datas = self.datas[self.index_train]
        self.train_labs = self.labs[self.index_train]

        self.test_datas = self.datas[self.index_test]
        self.test_labs = self.labs[self.index_test]

    def knn_one_sample(self,testData):
        # 计算训练样本的行数
        rowSize = self.train_datas.shape[0]
        # 计算训练样本和测试样本的差值
        diff = np.tile(testData, (rowSize, 1)) - self.train_datas
        # 计算差值的平方和
        sqrDiff = diff ** 2
        sqrDiffSum = sqrDiff.sum(axis=1)
        # 计算距离
        distances = sqrDiffSum ** 0.5
        # 对所得的距离从低到高进行排序
        sortDistance = distances.argsort()

        index_k = sortDistance[:self.k]
        count = {}
        
        for i in range(self.k):
            vote = self.train_labs[sortDistance[i]]
            # print(vote)
            count[vote] = count.get(vote, 0) + 1
        # 对类别出现的频数从高到低进行排序
        sortCount = sorted(count.items(), key=operator.itemgetter(1), reverse=True)
        
        # 返回出现频数最高的类别
        return sortCount[0][0],index_k,count
        
    def KNN_testSet(self):
        self.list_error = []
        N_test = self.test_datas.shape[0]
    
        N_right= 0
        for i in range(N_test):
            det_lab,_,_ =  self.knn_one_sample(self.test_datas[i])
            
            if det_lab == self.test_labs[i]:
                N_right += 1 #N_right +1
            else:
                self.list_error.append(i)
        
        # 计算准确率
        acc = N_right*100/N_test
        return acc 
    
   
    
    def plot_train_datas(self,sel_i):
        fig = plt.figure()
        # print("")
        # 标注训练样本点
        show_train_datas = self.datas_show[self.index_train,:]
        show_train_labs = self.train_labs
        unique_labs = np.unique(self.labs).tolist()
        
        for lab in unique_labs:
            index = np.where(show_train_labs==lab)[0]
            
            x = show_train_datas[index,0]
            y = show_train_datas[index,1]
            
            plt.scatter(x,y,s=56,c=self.dic_show[lab][0],marker=self.dic_show[lab][1],label=lab,alpha=0.5)
        plt.legend(loc='best')
        
        # 标注测试样本点（画圈）
        show_test_datas = self.datas_show[self.index_test,:]
        testData_show = show_test_datas[sel_i]   
        plt.scatter(x=testData_show[0],y=testData_show[1],s=205,c='none',marker='o',edgecolors='k')
        
        
        # 进行KNN分类
        testData = self.test_datas[sel_i]
        det_lab,index_K,count = self.knn_one_sample(testData)

        # 绘制圈出K个近邻
        for j in index_K.tolist():
            x = show_train_datas[j,0]
            y = show_train_datas[j,1]
            plt.scatter(x,y,s=200,c='none',marker='o',edgecolors='y')
        
        # 做出最终判断 绘制判定图标
        plt.scatter(x=testData_show[0],y=testData_show[1],s=65,c=self.dic_show[det_lab][0],marker=self.dic_show[det_lab][1],alpha=0.5)
        
        str_title = ""
        for k in count:
            str_title = str_title + "%s:%d "%(k,count[k])

        str_title= str_title + "\n det as " + det_lab        
        plt.title(str_title)

        # 绘制真实图标
        lab_true = self.test_labs[sel_i]
        plt.scatter(x=testData_show[0],y=testData_show[1],s=65,c=self.dic_show[lab_true][0],marker=self.dic_show[lab_true][1],alpha=0.5)

        if lab_true == det_lab:
            str_title = str_title + " True "
        else:
            str_title = str_title + " False "
        plt.title(str_title)

       
        return fig
        
  


if __name__ =="__main__":
    m_KNN = M_KNN()
    m_KNN.set_K(5)
    m_KNN.set_ratio(0.8)
    acc = m_KNN.KNN_testSet()
    dic = m_KNN.plot_train_datas(2)
    plt.show()
 
    print(acc)
        
        
