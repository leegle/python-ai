
import os
from PIL import Image
import numpy as np
def gfpgan_load(file_fpgan):
    from gfpgan import GFPGANer
    gfpgan_constructor = GFPGANer
    model = gfpgan_constructor(model_path=file_fpgan,upscale=1, arch='clean', channel_multiplier=2,bg_upsampler=None)
    return model

# 自定义函数修复人脸    
def gfpgan_fix_faces_model(model,pil_img):
    np_image  = np.array(pil_img)
    np_image_bgr = np_image[:, :, ::-1]
    cropped_faces, restored_faces, gfpgan_output_bgr = model.enhance(np_image_bgr, has_aligned=False, only_center_face=False, paste_back=True)
    np_image = gfpgan_output_bgr[:, :, ::-1]

    return Image.fromarray(np_image,'RGB')
file_fpgan = os.path.join('gfpgan','weights','GFPGANv1.4.pth')
if __name__ == "__main__":
    model = gfpgan_load(file_fpgan)
    img = Image.open("out.jpg")
    fix_img = gfpgan_fix_faces_model(model,img)

    fix_img.save("fix.png")

