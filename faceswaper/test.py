import datetime
import numpy as np
import os
import os.path as osp
import glob
import cv2
import insightface
from insightface.app import FaceAnalysis


def get_max_face(app,img):
    faces = app.get(img)
    if len(faces)<1:
        return False
    
    areas = []
    for face in faces:
        bbox = face['bbox']
        area =abs((bbox[0]-bbox[2])*(bbox[1]-bbox[3]))
        areas.append(area)    
    index = np.argmax(areas)
    return faces[index]

base_path = os.getcwd()
if __name__ == '__main__':
    
    # 人脸分析模型加载
    app = FaceAnalysis(name='buffalo_l',root=base_path)
    app.prepare(ctx_id=0, det_size=(640, 640))
    
    # 人脸转换模型加载
    name = os.path.join(base_path,'models',"inswapper_128.onnx")
    swapper = insightface.model_zoo.get_model(name,root = base_path)
    
    # 获取源人脸信息
    img_src = cv2.imread("baijingting.jpg")
    face_src = get_max_face(app,img_src)
    
    # 获取目标人脸信息
    img_tgt = cv2.imread("sunhonglei.jpg")
    face_tgt = get_max_face(app,img_tgt)
    
    if face_tgt is None or face_src is None:
        print("-----no face--------------")
        pass
    
    # 人脸转换    
    res = img_tgt.copy()
    res =  swapper.get(res, face_tgt, face_src, paste_back=True)

    # 保存并显示
    cv2.imwrite("out.jpg",res)
    cv2.imshow("out",res)
    cv2.waitKey(0)
    
   