import streamlit as st
import os
import numpy as np
import uuid
import dlib
import cv2
from src.util import det_draw_one_face,gen_face_name,file2vector,face_recognize
import time

#  一些全局参数

if not "flag_state" in st.session_state: 
    st.session_state['flag_state'] = 0

if not "flag_collect" in st.session_state:              
    st.session_state['flag_collect'] = 0

if not "flag_video" in st.session_state:  
    st.session_state['flag_video'] = 0


# 创建文件夹
path_faces = os.path.join("data","faces")
os.makedirs(path_faces,exist_ok=True)
path_vectors = os.path.join("data","vectors")
os.makedirs(path_vectors,exist_ok=True)

@st.cache_resource
def model_load():
    # 加载人脸特征提取器
    facerec = dlib.face_recognition_model_v1("dlib_face_recognition_resnet_model_v1.dat")
    # 加载人脸标志点检测器
    sp = dlib.shape_predictor("shape_predictor_5_face_landmarks.dat")
    # 创建人脸检测器
    det_face = dlib.get_frontal_face_detector()
     # 打开摄像头
    cap = cv2.VideoCapture(0)

    return facerec,sp,det_face,cap

facerec,sp,det_face,cap = model_load()


def make_folder():
    str_id = st.session_state.input_id
    folder_id = os.path.join(path_faces,str_id)
    os.makedirs(folder_id,exist_ok=True)
    print(str_id)

def set_state2face_det():
    st.session_state['flag_state'] = 1

def set_state2face_reg():
    st.session_state['flag_state' ]= 2

def clear_state():
    st.session_state['flag_state'] = 0


def set_flag_collect():
    st.session_state['flag_collect'] = 1

def start_video():
    st.session_state['flag_video'] = 1

def stop_video():
    st.session_state['flag_video'] = 0

def gen_face_vectors(st_info):
    list_face_ids = []
    for roots,dirs,files in os.walk(path_faces):
        for dir in dirs:
            list_face_ids.append(dir)
    
    print(list_face_ids)
    for face_id in list_face_ids:
        path_face = os.path.join(path_faces,face_id)
        
        path_vector = os.path.join(path_vectors,face_id)
        
        os.makedirs(path_vector,exist_ok=True)

        for roots,dirs,files in os.walk(path_face):
            for file in files:
                print("file",file)
                if file.endswith(".jpg"):

                    file_face = os.path.join(roots,file)
                    print("file_face",file_face)
                    file_vec = os.path.join(path_vectors,face_id,file[:-3]+"npy")
                    if os.path.exists(file_vec):
                        st_info.success(file_vec)
                        continue
            
                    img = cv2.imread(file_face)
                    img = cv2.cvtColor(img, cv2. COLOR_BGR2RGB)

                    # 存放的是已经截取好的人脸图片，所以在整图内检测标志点
                    img = np.array(img)
                    h,w,_ = np.shape(img)
                    rect = dlib.rectangle(0,0,w,h)
                    # 辅助人脸定位
                    shape = sp(img, rect)
                    # 获取128维人脸特征
                    face_vector = facerec.compute_face_descriptor(img,shape)
                    face_vector = file2vector(file_face,sp,facerec)
                    np.save(file_vec,face_vector)
                    st_info.success(file_vec)




st.title("人脸识别演示系统")

tab1, tab2,tab3 = st.tabs(["人脸采集", "人脸特征生成","人脸识别"])


with tab1:
    col_s_face_det, cols_e_face_det = st.columns(2)
    with col_s_face_det:
        st.button("启动人脸检测",on_click=set_state2face_det)
    with cols_e_face_det:
        st.button("停止人脸检测",on_click=clear_state)

    col_str_id, col_bt_collect = st.columns(2)    
    with col_str_id:
        str_id = st.text_input("人脸ID(英文)",value="yuhong",on_change=make_folder,key="input_id")
    with col_bt_collect:
        st.write("")
        st.write("")
        st.button("人脸采集",on_click=set_flag_collect)
   

with tab2:
    
    b_gen = st.button("人脸训练")
    if b_gen:
        st_info = st.empty()
        gen_face_vectors(st_info)
        st_info.success("训练结束")
       


with tab3:
    col_bt_s_reg,col_bt_e_reg =  st.columns(2)
   
    with col_bt_s_reg:
        st.button("启动人脸识别",on_click=set_state2face_reg)
    with col_bt_e_reg:
        st.button("停止人脸识别",on_click=clear_state)

    

st.write("-------------------------------------------------------------------------")

col1, col2 = st.columns(2)
with col1:
    st.button("摄像头打开",on_click=start_video)
with col2:
    st.button("摄像头停止",on_click=stop_video)


st_show = st.empty()
st_info = st.empty()

try:
    while st.session_state['flag_video']==1 :
        
        # 读取一帧图像
        success, img = cap.read()
        
        if st.session_state['flag_state'] ==1:

            img,max_rect,max_area= det_draw_one_face(img,det_face)
        
            if st.session_state['flag_collect']==1:
               
                if max_area>10:
                    # 截取人脸图像
                    roi = img[max_rect.top():max_rect.bottom(),max_rect.left():max_rect.right()]
                    # 生成文件名
                    str_id = st.session_state.input_id
                    os.makedirs(os.path.join(path_faces,str_id),exist_ok=True)
                    save_face_name = os.path.join(path_faces,str_id,gen_face_name(str_id))
                    # 文件保存
                    cv2.imwrite(save_face_name,roi)
                    st_info.success("保存为"+save_face_name)

                st.session_state['flag_collect'] = 0
                
                    
        elif st.session_state['flag_state'] ==2:
                
            # 人脸识别
        
            method = "dis_euc"
        
                
            # 转换为灰度
            img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # BGR 转 RGB
            img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


            # 检测人脸区域
            face_rects = det_face(img_gray, 0) 
            
            # 遍历检测的人脸
            for k, rect in enumerate(face_rects):
                
                # 画框
                cv2.rectangle(img, (rect.left(), rect.top()), (rect.right(), rect.bottom()), (255, 0, 0), 3)
                
                # 标志点检测
                shape = sp(img_rgb, rect)
                
                # 获取人脸特征
                face_vector = facerec.compute_face_descriptor(img_rgb, shape)
                
                str_id,out_sore = face_recognize(face_vector,path_vectors,method)

                if method == "dis_euc" and out_sore <0.45:
                    txt_show = "%s:%.2f"%(str_id,out_sore)
                elif method == "dis_euc" and out_sore >=0.45:
                    txt_show = "unknown:%.2f"%(out_sore)
                elif method == "dis_cos" and out_sore >0.95:
                    txt_show = "%s:%.2f"%(str_id,out_sore)
                elif  method == "dis_cos" and out_sore <=0.95:
                    txt_show = "unknown:%.2f"%(out_sore)
                else:
                    txt_show=" "

                # 绘制检测结果
                cv2.putText(img, txt_show, (rect.left()+5,rect.top()), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0,0,255), 2)


        # 图像显示
        img_show = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

        st_show.image(img_show)

        time.sleep(0.001)
except Exception as e:
    print(e)






